import styled from 'styled-components'

export const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    min-height: 100vh;
`

export const Container = styled.div`
    margin: 0 auto;
    max-width: 1420px;
    padding: 0 30px 0 30px;
`

export const StyledHeader = styled.header`
    //just to use ThemeProvider
    background-color: ${(props) => props.theme.bgc};
    color: #ffffff;
`
// just to use ThemeProvider
StyledHeader.defaultProps = {
  theme: {
    bgc: 'aqua'
  }
}

export const HeaderInner = styled.div`
    display: flex;
    align-items: flex-start;
    justify-content: space-between;
    padding: 15px 0 15px 0;
`

export const HeaderAuthorName = styled.h2`
    padding: 10px 0 10px 0;
    font-size: 20px;
    line-height: 24px;
    color: #ffffff;
`

export const Main = styled.main`
    padding: 25px 0 25px 0;
`

export const StyledFooter = styled.footer`
    margin: auto 0 0 0;
    border-top: 1px solid var(--color-main);
    padding: 25px 0 25px 0;
`

export const FooterInner = styled.div`
    display: flex;
    align-items: flex-start;
`

export const NoNoNoListenListen = styled.a`
    margin: 0 20px 0 0;
    border-radius: 5px;
    padding: 10px 10px 10px 10px;
    background-color: var(--color-main);
    color: #ffffff;
    cursor: pointer;
`

export const StyledThemeSelectors = styled.ul`
    display: flex;
    align-items: center;
    flex-wrap: wrap;
    margin: 0 -20px 0 -20px;
`

export const ThemeSelector = styled.li`
    display: flex;
    align-items: center;
    padding: 10px 20px 10px 20px;
`

export const ThemeSelectorHead = styled.h2`
    padding: 0 10px 0 0;
`

export const ThemeSelectorChanger = styled.div`
    flex-shrink: 0;
    border: 2px solid #ffffff;
    border-radius: 5px;
    padding: 5px 5px 5px 5px;
    min-width: 200px;
    color: var(--color-main);
    cursor: pointer;
`
