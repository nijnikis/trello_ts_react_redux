import React from 'react';
import { useSelector } from "react-redux";
import { ThemeProvider } from 'styled-components';


import PopAuth from "./components/PopAuth";
import Board from "./components/Board";
import TaskCard from "./components/TaskCard";
import Header from "./components/Header";
import Footer from "./components/Footer";
import {
  IRootState
} from './types'
import {
  Wrapper,
  Main,
  Container
} from "./styledComponentsStyles";


//just to use ThemeProvider
const theme = {
  bgc: "var(--color-main)"
};


const App: React.FC = () => {

  const state = useSelector<IRootState, IRootState>(state => state);

  const account = state.account;

  const taskPopupState = state.taskPopupState;
  

  return(
    <>
      <style>
        {
          `
            :root{
              --color-main:${account.mainTheme};
              --color-text-main:${account.textTheme};
            }
          `
        }
      </style>
      {
        account.name === null ?
          (
            <PopAuth />
          )
        :
          (
            <Wrapper>
              <ThemeProvider theme={theme}>
                <Header account={account} />
              </ThemeProvider>
              <Main>
                <Container>
                  <Board />
                </Container>
              </Main>
              <Footer />
              { taskPopupState.taskOpenedId && <TaskCard state={state} /> }
            </Wrapper>
          )
        }
    </>
  )
}


export default App
