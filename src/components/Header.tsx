import React from 'react';


import {
    IHeaderProps
} from '../types'
import {
    StyledHeader,
    Container,
    HeaderInner,
    HeaderAuthorName
} from "../styledComponentsStyles";
import ThemeSelectors from "./ThemeSelectors";


const Header: React.FC<IHeaderProps> = ({ account }: IHeaderProps) => {

    return (
        <StyledHeader>
            <Container>
                <HeaderInner>
                <HeaderAuthorName>Oh hi, {account.name}!</HeaderAuthorName>
                    <ThemeSelectors account={account} />
                </HeaderInner>
            </Container>
        </StyledHeader>
    )
}

export default Header
