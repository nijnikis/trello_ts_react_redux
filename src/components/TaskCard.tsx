// did not find any proper answer if should import KeyboardEvent or not
// some strange stuff happens if KeyboardEvent is imported
import React, { SyntheticEvent, useEffect, useCallback, useRef, useState } from 'react';
import { useDispatch } from "react-redux";


import './TaskCard.css';
import Comment from "./Comment";
import Pencil from "./svg/Pencil";
import Trash from "./svg/Trash";
import {
    ITaskCardProps
} from '../types';
import {
    toggleTask
} from "../redux/actions";
import {
    toggleEditTaskName,
    editTaskName,
    removeTask,
    editTaskDescr,
    toggleEditTaskDescr
} from "../redux/reducers/tasksReducer";
import {
    addComment
} from "../redux/reducers/commentsReducer";


const TaskCard: React.FC<ITaskCardProps> = ({ state }: ITaskCardProps) => {

    const dispatch = useDispatch()

    const taskNameNewInputRef = useRef<HTMLInputElement>(null)

    const taskDescrNewInputRef = useRef<HTMLInputElement>(null)

    const author = state.account.name

    const columns = state.board

    const task = state.tasksState.find(task => task.id === state.taskPopupState.taskOpenedId)!

    const comments = state.commentsState.filter(comment => (comment.taskId === task.id))
    
    const [taskDescrNewInputValue, setTaskDescrNewInputValue] = useState(task.descr);
    
    const [commentNewInputValue, setCommentNewInputValue] = useState('');
    
    const [taskNameNewInputValue, setTaskNameNewInputValue] = useState(task.name);


    const handleKeyUp = useCallback((event: KeyboardEvent) => {

        if (event.key === 'Escape') {

            dispatch(toggleTask(task.id))
            
        }

    }, [dispatch, task.id]);
    // }, []);

    useEffect(() => {

        document.addEventListener("keyup", handleKeyUp, false);

        return () => {
            document.removeEventListener("keyup", handleKeyUp, false);
        };

    }, [handleKeyUp]);
    // }, []);

    const taskRemoverClickHandler = (event: SyntheticEvent) => {

        event.preventDefault()
        
        if(author !== task.author){
            return alert(`Task name could only be removed by its author! Please, apply to ${task.author}.`)
        }

        dispatch(toggleTask(null))

        dispatch(removeTask(task.id))

    }

    const taskNameEditorClickHandler = (event: SyntheticEvent) => {

        event.preventDefault()
        
        if(author !== task.author){
            return alert(`Task name could only be changed by its author! Please, apply to ${task.author}.`)
        }

        dispatch(toggleEditTaskName(task.id))

        //waiting till new macrotask
        setTimeout(() => {
            taskNameNewInputRef.current!.focus()
        }, 0);

    }

    const taskCloserClickHandler = (event: SyntheticEvent) => {

        event.preventDefault()

        dispatch(toggleTask(task.id))

    }

    const taskNameNewInputHandler = (value: string) => {
        setTaskNameNewInputValue(value)
    }

    const taskNameNewSubmitHandler = (event: SyntheticEvent) => {

        event.preventDefault()

        const newTaskName = taskNameNewInputValue.trim()
        
        if(!newTaskName){

            dispatch(toggleEditTaskName(task.id))

            return alert('Task name should not be empty')

        }

        dispatch(editTaskName({id: task.id, name: newTaskName}))

    }

    const taskDescrEditorClickHandler = (event: SyntheticEvent) => {

        event.preventDefault()
        
        if(author !== task.author){
            return alert(`Task description could only be changed by its author! Please, apply to ${task.author}.`)
        }

        dispatch(toggleEditTaskDescr(task.id))

        setTimeout(() => {
            taskDescrNewInputRef.current!.focus()
        }, 0);

    }

    const taskDescrNewInputHandler = (value: string) => {
        setTaskDescrNewInputValue(value)
    }

    const taskDescrEditSubmitHandler = (event: SyntheticEvent) => {

        event.preventDefault()

        const newTaskDescr = taskDescrNewInputValue.trim()

        dispatch(editTaskDescr({id: task.id, text: newTaskDescr}));

    }

    const taskDescrRemoverClickHandler = (event: SyntheticEvent) => {

        event.preventDefault()
        
        if(author !== task.author){
            return alert(`Task description could only be changed by its author! Please, apply to ${task.author}.`)
        }
        
        setTaskDescrNewInputValue('')
        
        dispatch(editTaskDescr({id: task.id, text: ''}))

    }

    const commentNewInputHandler = (value: string) => {
        setCommentNewInputValue(value)
    }

    const commentAdderSubmitHandler = (event: SyntheticEvent) => {

        event.preventDefault()

        const newCommentText = commentNewInputValue.trim()
        
        if(!newCommentText){
            return alert('Comment should not be empty')
        }

        dispatch(addComment({taskId: task.id, author: author as string, text: newCommentText}))

        setCommentNewInputValue('')

    }
    

    return (
        <div className="task-popup">
            <div className="task-popup__bg"></div>
            <div className="task-popup__inner">
                <div className="task-popup__controller">
                    <button className="task-popup__remover button_alt" onClick={taskRemoverClickHandler}>
                        <Trash />
                    </button>
                    <div className="task-popup__name task-popup-name">
                        {
                        task.nameEdited ?
                            (
                                <form className="task-popup-name__changer task-popup-name-changer" onSubmit={taskNameNewSubmitHandler}>
                                    <input
                                        type="text"
                                        className="task-popup-name-changer__input"
                                        placeholder={task.name}
                                        spellCheck="false"
                                        ref={taskNameNewInputRef}
                                        value={taskNameNewInputValue}
                                        onChange={e => taskNameNewInputHandler((e.target as HTMLInputElement).value)}
                                    />
                                    <button className="task-popup-name-changer__submit button_alt">Ok</button>
                                </form>
                            )
                        :
                            (
                                <>
                                    <p className="task-popup-name__text">{task.name}</p>
                                    <button className="task-popup-name__edit button_alt" onClick={taskNameEditorClickHandler}>
                                        <Pencil />
                                    </button>
                                </>
                            )
                        }
                    </div>
                    <button className="task-popup__closer button_alt button_exit" onClick={taskCloserClickHandler}></button>
                </div>
                <div className="task-popup__content">
                    <p className="task-popup__column task-popup-column">
                        <span className="task-popup-column__name">Column</span>
                        <span className="task-popup-column__divider">:</span>
                        <span className="task-popup-column__value">{columns.find(column => task.columnId === column.id)!.name}</span>
                    </p>
                    <p className="task-popup__author task-popup-author">
                        <span className="task-popup-author__name">Author</span>
                        <span className="task-popup-author__divider">:</span>
                        <span className="task-popup-author__value">{task.author}</span>
                    </p>
                    <div className="task-popup__descr task-popup-descr">
                        <div className="task-popup-descr__head">
                            <span className="task-popup-descr__name">Description</span>
                            <span className="task-popup-descr__divider">:</span>
                        </div>
                        {
                        task.descrEdited ?
                            (
                                <form className="task-popup-descr__changer task-popup-descr-changer" onSubmit={taskDescrEditSubmitHandler}>
                                    <input
                                        type="text"
                                        className="task-popup-descr-changer__input"
                                        placeholder={taskDescrNewInputValue}
                                        spellCheck="false"
                                        value={taskDescrNewInputValue}
                                        ref={taskDescrNewInputRef}
                                        onChange={e => taskDescrNewInputHandler((e.target as HTMLInputElement).value)}
                                    />
                                    <button className="task-popup-descr-changer__submit button_alt">Ok</button>
                                </form>
                            )
                        :
                            (
                                <div className="task-popup-descr__inner">
                                    <p className="task-popup-descr__value">{task.descr}</p>
                                    <div className="task-popup-descr__controls">
                                        <button className="task-popup-descr__editor button_alt" onClick={taskDescrEditorClickHandler}>
                                            <Pencil />
                                        </button>
                                        <button className="task-popup-descr__remover button_alt" onClick={taskDescrRemoverClickHandler}>
                                            <Trash />
                                        </button>
                                    </div>
                                </div>
                            )
                        }
                    </div>
                    <div className="task-popup__comments task-popup-comments">
                        <p className="task-popup-comments__head">
                            <span className="task-popup-comments__name">Comments</span>
                            <span className="task-popup-comments__divider">:</span>
                        </p>
                        <ul className="task-popup-comments__list">
                            {
                                comments.map((comment) => {
                                    return(
                                        <li className="task-popup-comments__item" key={comment.id}>
                                            <Comment comment={comment} />
                                        </li>
                                    )
                                })
                            }
                        </ul>
                    </div>
                </div>
                <form className="task-popup__adder task-popup-adder" onSubmit={commentAdderSubmitHandler}>
                    <input
                        type="text"
                        className="task-popup-adder__input"
                        placeholder="New Comment"
                        spellCheck="false"
                        value={commentNewInputValue}
                        onChange={e => commentNewInputHandler((e.target as HTMLInputElement).value)}
                    />
                    <button className="task-popup-adder__submit button_alt">+</button>
                </form>
            </div>
        </div>
    )
}


export default TaskCard
