import React from 'react';
import { useDispatch } from "react-redux";
import Select, { ValueType } from 'react-select';


import {
    ISelectThemeOption,
    IThemeSelectorsProps
} from '../types'
import {
    setMainTheme,
    setTextTheme
} from "../redux/actions";
import {
    StyledThemeSelectors,
    ThemeSelectorHead,
    ThemeSelectorChanger,
    ThemeSelector
} from "../styledComponentsStyles";


const mainThemeOptions: ISelectThemeOption[] = [
    {
        value: '#000000',
        label: 'Black'
    },
    {
        value: '#997ABF',
        label: 'Purple'
    },
    {
        value: '#4285f4',
        label: 'Blue'
    },
]

const mainTextOptions: ISelectThemeOption[] = [
    {
        value: '#000000',
        label: 'Black'
    },
    {
        value: '#997ABF',
        label: 'Purple'
    },
    {
        value: '#4285f4',
        label: 'Blue'
    },
]


const ThemeSelectors: React.FC<IThemeSelectorsProps> = ({ account }: IThemeSelectorsProps) => {

    const dispatch = useDispatch()

    const accountMainTheme = account.mainTheme

    const accountTextTheme = account.textTheme

    // const mainThemeSelectorChangeHandler = (selectedOption: ValueType<ISelectThemeOption>) => {
    // need to add 'boolean' after library update.
    const mainThemeSelectorChangeHandler = (selectedOption: ValueType<ISelectThemeOption, boolean>) => {
        
        const selectedOptionValue: string = (selectedOption as ISelectThemeOption).value;
    
        if(selectedOptionValue === accountMainTheme){
            return alert('This theme is already selected')
        }
    
        dispatch(setMainTheme(selectedOptionValue))
    
    }
    
    // const textThemeSelectorChangeHandler = (selectedOption: ValueType<ISelectThemeOption>) => {
    // need to add 'boolean' after library update.
    const textThemeSelectorChangeHandler = (selectedOption: ValueType<ISelectThemeOption, boolean>) => {
        
        const selectedOptionValue: string = (selectedOption as ISelectThemeOption).value;
    
        if(selectedOptionValue === accountTextTheme){
            return alert('This theme is already selected')
        }
    
        dispatch(setTextTheme(selectedOptionValue))
    
    }


    return (
        <StyledThemeSelectors>
            <ThemeSelector>
                <ThemeSelectorHead>Theme</ThemeSelectorHead>
                <ThemeSelectorChanger>
                <Select
                    onChange={mainThemeSelectorChangeHandler}
                    defaultValue={mainThemeOptions.find(mainThemeOption => mainThemeOption.value === accountMainTheme)}
                    options={mainThemeOptions}
                />
                </ThemeSelectorChanger>
            </ThemeSelector>
            <ThemeSelector>
                <ThemeSelectorHead>Text</ThemeSelectorHead>
                <ThemeSelectorChanger>
                <Select
                    onChange={textThemeSelectorChangeHandler}
                    defaultValue={mainTextOptions.find(mainTextOption => mainTextOption.value === accountTextTheme)}
                    options={mainTextOptions}
                />
                </ThemeSelectorChanger>
            </ThemeSelector>
        </StyledThemeSelectors>
    )
}

export default ThemeSelectors

