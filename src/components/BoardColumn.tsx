import React, { SyntheticEvent, useState, useRef, useMemo } from 'react';
import { useDispatch, useSelector } from "react-redux";


import './BoardColumn.css';
import Task from "./Task";
import Pencil from "./svg/Pencil";
import {
    IBoardColumnProps,
    IRootState,
    AccountName,
    TasksState
} from '../types'
import {
    toggleEditColumnName,
    editColumnName
} from "../redux/actions";
import {
    addTask
} from "../redux/reducers/tasksReducer";


const BoardColumn: React.FC<IBoardColumnProps> = ({ column }: IBoardColumnProps) => {

    const tasks = useSelector<IRootState, TasksState>(state => state.tasksState)

    const tasksFiltered  = useMemo(() => tasks.filter(task => task.columnId === column.id), [tasks, column.id])

    const author = useSelector<IRootState, AccountName>(state => state.account.name)

    const dispatch = useDispatch()

    const columnNameNewInputRef = useRef<HTMLInputElement>(null)
    
    const [columnNameNewInputValue, setColumnNameNewInputValue] = useState(column.name);

    const columnNameNewInputHandler = (value: string) => {
        setColumnNameNewInputValue(value)
    }
    
    const [boardColumnAdderInputValue, setBoardColumnAdderInputValue] = useState('')

    const boardColumnAdderInputHandler = (value: string) => {
        setBoardColumnAdderInputValue(value)
    }

    const taskAdderSubmitHandler = (event: SyntheticEvent) => {

        event.preventDefault()

        const newTaskName = boardColumnAdderInputValue.trim()
        
        if(!newTaskName){
            return alert('New task name should not be empty')
        }

        setBoardColumnAdderInputValue('')

        dispatch(addTask({columnId: column.id, name: newTaskName, author: author as string}))

    }

    const columnNameEditorClickHandler = (event: SyntheticEvent) => {

        event.preventDefault()

        dispatch(toggleEditColumnName(column.id))

        //waiting till new maсrotask
        setTimeout(() => {
            columnNameNewInputRef.current!.focus()
        }, 0)

    }

    const columnNameNewSubmitHandler = (event: SyntheticEvent) => {

        event.preventDefault()

        const newColumnName = columnNameNewInputValue.trim()
        
        if(!newColumnName){
            dispatch(toggleEditColumnName(column.id))
            return alert('Column name should not be empty')
        }

        dispatch(editColumnName(column.id, newColumnName))

    }

    return (
        <div className="board-column">
            <div className="board-column__name board-column-name">
                {
                column.edited ?
                    (
                        <form className="board-column-name__changer board-column-name-changer" onSubmit={columnNameNewSubmitHandler}>
                            <input
                                type="text"
                                className="board-column-name-changer__input"
                                placeholder={column.name}
                                spellCheck="false"
                                value={columnNameNewInputValue}
                                ref={columnNameNewInputRef}
                                onChange={e => columnNameNewInputHandler((e.target as HTMLInputElement).value)}
                            />
                            <button className="board-column-name-changer__submit">Ok</button>
                        </form>
                    )
                :
                    (
                        <>
                            <p className="board-column-name__text">{column.name}</p>
                            <button className="board-column-name__edit button_alt" onClick={columnNameEditorClickHandler}>
                                <Pencil />
                            </button>
                        </>
                    )
                }
            </div>
            <ul className="board-column__list">
                {
                    tasksFiltered.map((task) => {
                        return(
                            <li className="board-column__item" key={task.id}>
                                <Task task={task} />
                            </li>
                        )
                    })
                }
            </ul>
            <form className="board-column__adder board-column-adder" onSubmit={taskAdderSubmitHandler}>
                <input
                    type="text"
                    className="board-column-adder__input"
                    placeholder="New Task"
                    spellCheck="false"
                    value={boardColumnAdderInputValue}
                    onChange={e => boardColumnAdderInputHandler((e.target as HTMLInputElement).value)}
                />
                <button className="board-column-adder__submit button_alt">+</button>
            </form>
        </div>
    )
}


export default BoardColumn
