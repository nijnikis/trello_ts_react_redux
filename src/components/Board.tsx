import React from 'react';
import { useSelector } from "react-redux";


import './Board.css';
import BoardColumn from "./BoardColumn";
import {
    IRootState,
    IBoardState
} from '../types'


const Board: React.FC = () => {

    const columns = useSelector<IRootState, IBoardState>(state => state.board);

    return (
        <ul className="board">
            {
                columns.map((column) => {
                    return(
                        <li className="board__column" key={column.id}>
                            <BoardColumn column={column} />
                        </li>
                    )
                })
            }
        </ul>
    )
}


export default Board

