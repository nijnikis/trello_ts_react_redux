import React, { SyntheticEvent, useState, useRef } from 'react';
import { useDispatch, useSelector } from "react-redux";


import './Comment.css';
import Pencil from "./svg/Pencil";
import Trash from "./svg/Trash";
import {
    ICommentProps,
    IRootState,
    AccountName
} from '../types';
import {
    toggleEditCommentText,
    editCommentText,
    removeComment
} from "../redux/reducers/commentsReducer";


const Comment: React.FC<ICommentProps> = ({ comment }: ICommentProps) => {

    const dispatch = useDispatch()

    const author = useSelector<IRootState, AccountName>(state => state.account.name)

    const commentTextNewInputRef = useRef<HTMLInputElement>(null)
    
    const [commentTextNewInputValue, setCommentTextNewInputValue] = useState(comment.text);

    const commentAuthor = comment.author

    const commentTextNewInputHandler = (value: string) => {
        setCommentTextNewInputValue(value)
    }

    const commentEditSubmitHandler = (event: SyntheticEvent) => {

        event.preventDefault()
        
        if(author !== commentAuthor){
            return alert(`Comment could only be changed by its author! Please, apply to ${commentAuthor}.`)
        }

        const newCommentText = commentTextNewInputValue.trim()
        
        if(!newCommentText){

            dispatch(toggleEditCommentText({id: comment.id}))

            return alert('Comment text should not be empty')
            
        }

        dispatch(editCommentText({id: comment.id, text:newCommentText}))

    }

    const commentEditorClickHandler = (event: SyntheticEvent) => {

        event.preventDefault()
        
        if(author !== commentAuthor){
            return alert(`Comment could only be changed by its author! Please, apply to ${commentAuthor}.`)
        }

        dispatch(toggleEditCommentText({id: comment.id}))

        setTimeout(() => {
            commentTextNewInputRef.current!.focus()
        }, );

    }

    const commentRemoverClickHandler = (event: SyntheticEvent) => {

        event.preventDefault()
        
        if(author !== commentAuthor){
            return alert(`Comment could only be removed by its author! Please, apply to ${commentAuthor}.`)
        }
        
        dispatch(removeComment({id: comment.id}))

    }

    return (
        <div className="comment">
            <p className="comment__author comment-author">
                <span className="comment-author__value">{comment.author}</span>
                <span className="comment-author__divider">:</span>
            </p>
            {
            comment.edited ?
                (
                    <form className="comment__changer comment-changer" onSubmit={commentEditSubmitHandler}>
                        <input
                            type="text"
                            className="comment-changer__input"
                            placeholder={comment.text}
                            spellCheck="false"
                            value={commentTextNewInputValue}
                            ref={commentTextNewInputRef}
                            onChange={e => commentTextNewInputHandler((e.target as HTMLInputElement).value)}
                        />
                        <button className="comment-changer__submit button_alt">Ok</button>
                    </form>
                )
            :
                (
                    <div className="comment__inner">
                        <p className="comment__value">{comment.text}</p>
                        <div className="comment__controls">
                            <button className="comment__editor button_alt" onClick={commentEditorClickHandler}>
                                <Pencil />
                            </button>
                            <button className="comment__remover button_alt" onClick={commentRemoverClickHandler}>
                                <Trash />
                            </button>
                        </div>
                    </div>
                )
            }
        </div>
    )
}

export default Comment
