import React, { SyntheticEvent, useState } from 'react';
import { useDispatch } from "react-redux";


import './PopAuth.css';
import {
    setAccountName
} from "../redux/actions";


const PopAuth: React.FC = () => {

    const dispatch = useDispatch()
    
    const [loginFormNameInputValue, setLoginFormNameInputValue] = useState('');

    const loginFormNameInputHandler = (value: string) => {
        setLoginFormNameInputValue(value)
    }

    const submitHandler = (event: SyntheticEvent) => {

        event.preventDefault()

        const formInputName = loginFormNameInputValue.trim()
        
        if(!formInputName){
            return alert('Account name should not be empty')
        }

        dispatch(setAccountName(formInputName))

    }

    return(
        <div className="login-popup">
            <div className="login-popup__card">
                <form className="login-popup__form login-form" onSubmit={submitHandler} >
                    <h2 className="login-form__head">What's your name?</h2>
                    <input
                        className="login-form__name-field"
                        type="text"
                        placeholder="My name is Jeff"
                        spellCheck="false"
                        value={loginFormNameInputValue}
                        onChange={e => loginFormNameInputHandler((e.target as HTMLInputElement).value)}
                    />
                    <button className="login-form__submit button_main">Ok</button>
                </form>
            </div>
        </div>
    )
}


export default PopAuth
