
import React from 'react';



const Trash: React.FC = () => {

    return (
        <svg width="14" height="15" viewBox="0 0 14 15" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M13 3H12H11V2C11 1 10 0 9 0H5C4 0 3 1 3 2V3H1C0.570459 3 0 3.57152 0 4C0 4.42848 0.570459 5 1 5H2V14C2 14.4285 2.57046 15 3 15H5.44444H8.55556L11 15C11.4295 15 12 14.4285 12 14V5H13C13.4295 5 14 4.42848 14 4C14 3.57152 13.4295 3 13 3ZM5 2L9 2V3H8.55556H5.44444H5V2ZM4 5H5V13L4 13V5ZM6 5H8V13H6V5ZM10 13H9V5H10V13Z" fill="var(--color-text-main)"/>
        </svg>
    )
}

export default Trash