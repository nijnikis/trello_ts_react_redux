import React, { SyntheticEvent } from 'react';
import { useDispatch } from "react-redux";


import {
    StyledFooter,
    Container,
    FooterInner,
    NoNoNoListenListen
} from "../styledComponentsStyles";
import {
  setAccountName
} from "../redux/actions";


const Footer: React.FC = () => {
  
  const dispatch = useDispatch()


  const accountNameEditorClickHandler = (event: SyntheticEvent) => {
  
      event.preventDefault()
  
      dispatch(setAccountName(null))
  
  }
  

  return (
    <StyledFooter>
      <Container>
        <FooterInner>
          <NoNoNoListenListen as='button' onClick={accountNameEditorClickHandler}>That's not my name, actually...</NoNoNoListenListen>
        </FooterInner>
      </Container>
    </StyledFooter>
  )
}

export default Footer
