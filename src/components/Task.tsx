import React, { SyntheticEvent } from 'react';
import { useDispatch, useSelector } from "react-redux";
import Select, { ValueType } from 'react-select';


import './Task.css';
import Chat from "./svg/Chat";
import {
    ISelectColumnOption,
    ITaskProps,
    IRootState,
    IBoardState,
    CommentsState
} from '../types';
import {
    toggleTask
} from "../redux/actions";
import {
    editTaskColumn
} from "../redux/reducers/tasksReducer";


// non-working walkaround
// type ValueType<OptionType extends OptionTypeBase> = OptionType | null | undefined;


const Task: React.FC<ITaskProps> = ({ task }: ITaskProps) => {

    const dispatch = useDispatch()

    //to memoize
    const columns = useSelector<IRootState, IBoardState>(state => state.board);

    //to memoize
    const comments = useSelector<IRootState, CommentsState>(state => state.commentsState);

    const taskMoverOptions: ISelectColumnOption[] = columns?.map((column) => {
        return{
            value: column.id,
            label: column.name
        }
    })

    const taskInfoClickHandler = (event: SyntheticEvent) => {

        event.preventDefault()

        dispatch(toggleTask(task.id))

    }

    // const taskMoverChangeHandler = (selectedOption: ValueType<ISelectColumnOption>) => {
    // need to add 'boolean' after library update.
    const taskMoverChangeHandler = (selectedOption: ValueType<ISelectColumnOption, boolean>) => {

        // working walkaround
        // const selectedOptionRepaired: ISelectColumnOption = {
        //     value: {...selectedOption}.value!,
        //     label: {...selectedOption}.label!
        // }

        // dispatch(editTaskColumn(taskId: task.id, columnId: selectedOptionRepaired.value))


        // another working (walkaround???)
        const selectedOptionValue: number = (selectedOption as ISelectColumnOption).value

        if(selectedOptionValue === task.columnId){
            return alert('Task is already at this board')
        }

        dispatch(editTaskColumn({id: task.id, columnId: selectedOptionValue}))

    }

    return (
        <div className="task">
            <button className="task__info" onClick={taskInfoClickHandler}>
                <p className="task__name">{task.name}</p>
                <div className="task__comments task-comments">
                    <p className="task-comments__number">{comments?.filter(commment => (commment.taskId === task.id)).length || 0}</p>
                    <div className="task-comments__vis">
                        <Chat />
                    </div>
                </div>
            </button>
            <Select
                className="task__mover"
                onChange={taskMoverChangeHandler}
                defaultValue={taskMoverOptions.find(taskMoverOption => taskMoverOption.value === task.columnId)}
                options={taskMoverOptions}
            />
        </div>
    )
}


export default Task
