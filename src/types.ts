import {
  SET_ACCOUNT_NAME,
  SET_ACCOUNT_MAIN_THEME,
  SET_ACCOUNT_TEXT_THEME,
  TOGGLE_EDIT_COLUMN_NAME,
  EDIT_COLUMN_NAME,
  TOGGLE_TASK
} from './redux/actionTypes'

// accountState
export type AccountName = string | null

export interface IAccountState {
  name: AccountName;
  mainTheme: string;
  textTheme: string;
}

// commentssState
export interface IComment {
  id: number;
  taskId: number;
  author: string;
  text: string;
  edited: boolean;
}

export type CommentsState = IComment[]

// taskPopupState
export type TaskOpenedId = number | null

export interface ITaskPopupState {
  taskOpenedId: TaskOpenedId;
}

// tasksState
export interface ITask {
  id: number;
  columnId: number;
  name: string;
  nameEdited: boolean;
  author: string;
  descr: string;
  descrEdited: boolean;
}

export type TasksState = ITask[]

// boardState
export interface IColumn {
  id: number;
  name: string;
  edited: boolean;
}

export type IBoardState = IColumn[]

// rootState
export interface IRootState {
  account: IAccountState;
  commentsState: CommentsState;
  taskPopupState: ITaskPopupState;
  tasksState: TasksState;
  board: IBoardState;
}

// accountActions
export interface ISetAccountName {
  type: typeof SET_ACCOUNT_NAME;
  payload: AccountName;
}

export interface ISetAccountMainTheme {
  type: typeof SET_ACCOUNT_MAIN_THEME;
  payload: string;
}

export interface ISetAccountTextTheme {
  type: typeof SET_ACCOUNT_TEXT_THEME;
  payload: string;
}

export type AccountActions = ISetAccountName | ISetAccountMainTheme | ISetAccountTextTheme

// taskPopupActions
export interface IToggleTask {
  type: typeof TOGGLE_TASK;
  payload: TaskOpenedId;
}

export type TaskPopupActions =
    | IToggleTask

// boardActions
export interface IToggleEditColumnName {
  type: typeof TOGGLE_EDIT_COLUMN_NAME;
  payload: number;
}

export interface IEditColumnName {
  type: typeof EDIT_COLUMN_NAME;
  payload: {
    id: number;
    name: string;
  };
}

export type ColumnActions = IToggleEditColumnName | IEditColumnName

export type BoardActions = ColumnActions

// allActions
export type AppActions = AccountActions | TaskPopupActions | BoardActions

// props
export interface IHeaderProps {
  account: IAccountState;
}

export interface IThemeSelectorsProps {
  account: IAccountState;
}

export interface IBoardColumnProps {
  column: IColumn;
}

export interface ITaskProps {
  task: ITask;
}

export interface ICommentProps {
  comment: IComment;
}

export interface ITaskCardProps {
  state: IRootState;
}

// accountReducer
export interface IAccountHandler {
  (arg0: IAccountState, arg1: AccountActions): IAccountState
}

// taskPopupReducer
export interface ITaskPopupHandler {
  (arg0: ITaskPopupState, arg1: TaskPopupActions): ITaskPopupState
}

// boardReducer
export interface IBoardHandler {
  (arg0: IBoardState, arg1: BoardActions): IBoardState
}

// different
export interface ISelectColumnOption {
  value: number;
  label: string;
}

export interface ISelectThemeOption {
  value: string;
  label: string;
}
