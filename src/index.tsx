import React from 'react';
import { render } from 'react-dom';
import { Provider } from "react-redux";


import './index.css';
import App from './App';
import { store } from './redux/store';
import * as serviceWorker from './serviceWorker';


const app = (
  <Provider store={store} >
    <App />
  </Provider>
)

render(app, document.getElementById('root'));

serviceWorker.unregister();
