import {
  SET_ACCOUNT_NAME,
  SET_ACCOUNT_MAIN_THEME,
  SET_ACCOUNT_TEXT_THEME,
  TOGGLE_EDIT_COLUMN_NAME,
  EDIT_COLUMN_NAME,
  TOGGLE_TASK
} from './actionTypes'
import {
  AccountName,
  ISetAccountName,
  IToggleEditColumnName,
  IEditColumnName,
  IToggleTask,
  ISetAccountMainTheme,
  ISetAccountTextTheme,
  TaskOpenedId
} from '../types'

// accountActions
export function setAccountName(name: AccountName): ISetAccountName {
  return {
    type: SET_ACCOUNT_NAME,
    payload: name
  }
}

export function setMainTheme(mainTheme: string): ISetAccountMainTheme {
  return {
    type: SET_ACCOUNT_MAIN_THEME,
    payload: mainTheme
  }
}

export function setTextTheme(textTheme: string): ISetAccountTextTheme {
  return {
    type: SET_ACCOUNT_TEXT_THEME,
    payload: textTheme
  }
}

// boardActions
export function toggleEditColumnName(id: number): IToggleEditColumnName {
  return {
    type: TOGGLE_EDIT_COLUMN_NAME,
    payload: id
  }
}

export function editColumnName(id: number, name: string): IEditColumnName {
  return {
    type: EDIT_COLUMN_NAME,
    payload: {
      id,
      name
    }
  }
}

// taskPopupActions
export function toggleTask(id: TaskOpenedId): IToggleTask {
  return {
    type: TOGGLE_TASK,
    payload: id
  }
}
