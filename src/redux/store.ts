import { configureStore } from '@reduxjs/toolkit'

import { rootReducer } from './rootReducer'

// sloution by God blessed mr.Abramov Himself
const localReduxState = localStorage.getItem('reduxState')

const persistedState = localReduxState ? JSON.parse(localReduxState) : {}

// eslint-disable-next-line import/prefer-default-export
export const store = configureStore({
  reducer: rootReducer,
  preloadedState: persistedState
})

store.subscribe(() => {
  localStorage.setItem('reduxState', JSON.stringify(store.getState()))
})
