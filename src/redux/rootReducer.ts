import { combineReducers } from 'redux'

import { accountReducer } from './reducers/accountReducer'
import commentsReducer from './reducers/commentsReducer'
import tasksReducer from './reducers/tasksReducer'
import { taskPopupReducer } from './reducers/taskPopupReducer'
import { boardReducer } from './reducers/boardReducer'

// eslint-disable-next-line import/prefer-default-export
export const rootReducer = combineReducers({
  account: accountReducer(),
  commentsState: commentsReducer,
  taskPopupState: taskPopupReducer(),
  tasksState: tasksReducer,
  board: boardReducer()
})
