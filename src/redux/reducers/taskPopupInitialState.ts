import {
  ITaskPopupState
} from '../../types'

export const taskPopupInitialState: ITaskPopupState = {
  taskOpenedId: null
}
