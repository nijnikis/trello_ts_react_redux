import {
  IBoardState,
  IEditColumnName
} from '../../../types'

export default function editColumnNameHandler(state: IBoardState, action: IEditColumnName) {
  return state.map((column) => (column.id === action.payload.id
    ? {
      ...column,
      name: action.payload.name,
      edited: false
    }
    : column))
}
