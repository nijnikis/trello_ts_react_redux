import {
  IBoardState,
  IToggleEditColumnName
} from '../../../types'

export default function toggleEditColumnNameHandler(
  state: IBoardState,
  action: IToggleEditColumnName
) {
  return state.map((column) => (column.id === action.payload
    ? {
      ...column,
      edited: !column.edited
    }
    : column))
}
