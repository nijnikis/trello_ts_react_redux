import {
  BoardActions,
  IBoardHandler
} from '../../types'
import { boardInitialState } from './boardInitialState'
import { boardActionHandlersCollection } from './boardActionHandlersCollection'

export const boardReducer = (
  initialState = boardInitialState,
  handlersCollection = boardActionHandlersCollection
) => (state = initialState, action: BoardActions) => {
  const handler = handlersCollection[action.type] as IBoardHandler

  return handler ? handler(state, action) : state
}

// export const boardReducer = (state = boardInitialState, action: BoardActions): IBoardState => {
//   switch (action.type) {
//     case TOGGLE_EDIT_COLUMN_NAME:
//       return state.map((column) => (column.id === action.payload
//         ? {
//           ...column,
//           edited: !column.edited
//         }
//         : column));
//     case EDIT_COLUMN_NAME:
//       return state.map((column) => (column.id === action.payload.id
//         ? {
//           ...column,
//           name: action.payload.name,
//           edited: false
//         }
//         : column));
//     default:
//       return state;
//   }
// };
