import {
  TasksState
} from '../../types'

// eslint-disable-next-line import/prefer-default-export
export const tasksInitialState: TasksState = [
  {
    id: 123,
    columnId: 1,
    name: 'interesting task',
    nameEdited: false,
    author: 'Jeff',
    descr: '',
    descrEdited: false
  },
  {
    id: 234,
    columnId: 1,
    name: 'important task',
    nameEdited: false,
    author: 'Jeff',
    descr: '',
    descrEdited: false
  },
  {
    id: 345,
    columnId: 1,
    name: 'burning from yesterday task',
    nameEdited: false,
    author: 'Jeff',
    descr: 'lorem lorem lorem lorem lorem lorem lorem lorem toyota ipsum',
    descrEdited: false
  }
]
