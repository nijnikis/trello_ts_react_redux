import {
  IAccountState,
  ISetAccountTextTheme
} from '../../../types'

export default function setAccountTextThemeHandler(
  state: IAccountState,
  action: ISetAccountTextTheme
) {
  return { ...state, textTheme: action.payload }
}
