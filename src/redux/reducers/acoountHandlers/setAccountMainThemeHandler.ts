import {
  IAccountState,
  ISetAccountMainTheme
} from '../../../types'

export default function setAccountMainThemeHandler(
  state: IAccountState,
  action: ISetAccountMainTheme
) {
  return { ...state, mainTheme: action.payload }
}
