import {
  IAccountState,
  ISetAccountName
} from '../../../types'

export default function setAccountNameHandler(state: IAccountState, action: ISetAccountName) {
  return { ...state, name: action.payload }
}
