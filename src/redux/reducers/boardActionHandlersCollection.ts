import {
  TOGGLE_EDIT_COLUMN_NAME,
  EDIT_COLUMN_NAME
} from '../actionTypes'
import editColumnNameHandler from './boardHandlers/editColumnNameHandler'
import toggleEditColumnNameHandler from './boardHandlers/toggleEditColumnNameHandler'

export const boardActionHandlersCollection = {
  [EDIT_COLUMN_NAME]: editColumnNameHandler,
  [TOGGLE_EDIT_COLUMN_NAME]: toggleEditColumnNameHandler
}
