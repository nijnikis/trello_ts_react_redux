import {
  CommentsState
} from '../../types'

export const commentsInitialState: CommentsState = [
  {
    id: 98765346,
    taskId: 345,
    author: 'Jeff',
    text: 'Ma namma Jaaff',
    edited: false
  },
  {
    id: 987346,
    taskId: 345,
    author: 'Mark',
    text: 'I did nut hit her! I did nut.',
    edited: false
  },
  {
    id: 98763405346,
    taskId: 345,
    author: 'Pavel Techniks',
    text: 'Ne ne ne ne, net slov, pobeda za Brolom.',
    edited: false
  }
]
