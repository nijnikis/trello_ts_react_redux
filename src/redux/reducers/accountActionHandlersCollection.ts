import {
  SET_ACCOUNT_NAME,
  SET_ACCOUNT_MAIN_THEME,
  SET_ACCOUNT_TEXT_THEME
} from '../actionTypes'
import setAccountNameHandler from './acoountHandlers/setAccountNameHandler'
import setAccountMainThemeHandler from './acoountHandlers/setAccountMainThemeHandler'
import setAccountTextThemeHandler from './acoountHandlers/setAccountTextThemeHandler'

export const accountActionHandlersCollection = {
  [SET_ACCOUNT_NAME]: setAccountNameHandler,
  [SET_ACCOUNT_MAIN_THEME]: setAccountMainThemeHandler,
  [SET_ACCOUNT_TEXT_THEME]: setAccountTextThemeHandler
}
