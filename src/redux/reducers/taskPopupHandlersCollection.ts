import {
  TOGGLE_TASK
} from '../actionTypes'
import toggleTaskHandler from './taskPopupHandlers/toggleTaskHandler'

export const taskPopupHandlersCollection = {
  [TOGGLE_TASK]: toggleTaskHandler
}
