import {
  TaskPopupActions,
  ITaskPopupHandler
} from '../../types'
import { taskPopupInitialState } from './taskPopupInitialState'
import { taskPopupHandlersCollection } from './taskPopupHandlersCollection'

export const taskPopupReducer = (
  initialState = taskPopupInitialState,
  handlersCollection = taskPopupHandlersCollection
) => (state = initialState, action: TaskPopupActions) => {
  const handler = handlersCollection[action.type] as ITaskPopupHandler

  return handler ? handler(state, action) : state
}

// export const taskPopupReducer =
//   (state = taskPopupInitialState, action: TaskPopupActions): ITaskPopupState => {
//     switch (action.type) {
//       case TOGGLE_TASK:
//         return {
//           taskOpenedId: state.taskOpenedId ? null : action.payload,
//         };
//       default:
//         // combineReducers() probes your reducers to check
//         // for common mistakes before actually using them.
//         // Don’t worry about it! (c) https://github.com/reduxjs/redux/issues/729
//         // prooved: with no combineReducers there is no
//         // triple default log with undefined in the console
//         return state;
//     }
// };
