import {
  AccountActions,
  IAccountHandler
} from '../../types'
import { accountInitialState } from './accountInitialState'
import { accountActionHandlersCollection } from './accountActionHandlersCollection'

export const accountReducer = (
  initialState = accountInitialState,
  handlersCollection = accountActionHandlersCollection
) => (state = initialState, action: AccountActions) => {
  const handler = handlersCollection[action.type] as IAccountHandler

  return handler ? handler(state, action) : state
}
