import { createSlice } from '@reduxjs/toolkit'

import { commentsInitialState } from './commentsInitialState'

export const commentsSlice = createSlice({
  name: 'commentsReducer',
  initialState: commentsInitialState,
  reducers: {
    toggleEditCommentText: (state, action) => (
      state.map((comment) => (comment.id === action.payload.id
        ? {
          ...comment,
          edited: !comment.edited
        }
        : comment))
    ),

    editCommentText: (state, action) => state.map((comment) => (comment.id === action.payload.id
      ? {
        ...comment,
        text: action.payload.text,
        edited: false
      }
      : comment)),

    removeComment: (state, action) => state.filter((comment) => comment.id !== action.payload.id),

    addComment: (state, action) => {
      const commentNewId = new Date().getTime()
      return [
        ...state,
        {
          id: commentNewId,
          taskId: action.payload.taskId,
          author: action.payload.author,
          text: action.payload.text,
          edited: false
        }
      ]
    }
  }
})

// export const { commentsActions, commentsReducer } = commentsSlice

export const {
  toggleEditCommentText,
  editCommentText,
  removeComment,
  addComment
} = commentsSlice.actions

export default commentsSlice.reducer
