import { createSlice } from '@reduxjs/toolkit'

import { tasksInitialState } from './tasksInitialState'

export const tasksSlice = createSlice({
  name: 'tasksReducer',
  initialState: tasksInitialState,
  reducers: {
    addTask: (state, action) => {
      const taskNewId = new Date().getTime()
      return [
        ...state,
        {
          // create all fields at the very beginning to prevent memory leak
          id: taskNewId,
          columnId: action.payload.columnId,
          name: action.payload.name,
          author: action.payload.author,
          descr: '',
          descrEdited: false,
          nameEdited: false
        }
      ]
    },
    editTaskColumn: (state, action) => state.map((task) => (task.id === action.payload.id
      ? {
        ...task,
        columnId: action.payload.columnId
      }
      : task)),
    editTaskDescr: (state, action) => state.map((task) => (task.id === action.payload.id
      ? {
        ...task,
        descr: action.payload.text,
        descrEdited: false
      }
      : task)),
    editTaskName: (state, action) => state.map((task) => (task.id === action.payload.id
      ? {
        ...task,
        name: action.payload.name,
        nameEdited: false
      }
      : task)),
    removeTask: (state, action) => state.filter((task) => task.id !== action.payload),
    toggleEditTaskDescr: (state, action) => state.map((task) => (task.id === action.payload
      ? {
        ...task,
        descrEdited: !task.descrEdited
      }
      : task)),
    toggleEditTaskName: (state, action) => state.map((task) => (task.id === action.payload
      ? {
        ...task,
        nameEdited: !task.nameEdited
      }
      : task))
  }
})

// export const { commentsActions, commentsReducer } = commentsSlice

export const {
  addTask,
  editTaskColumn,
  editTaskDescr,
  editTaskName,
  removeTask,
  toggleEditTaskDescr,
  toggleEditTaskName
} = tasksSlice.actions

export default tasksSlice.reducer
