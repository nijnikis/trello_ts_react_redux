import {
  IBoardState
} from '../../types'

export const boardInitialState: IBoardState = [
  {
    id: 1,
    name: 'ToDo',
    edited: false
  },
  {
    id: 2,
    name: 'In Progress',
    edited: false
  },
  {
    id: 3,
    name: 'Testing',
    edited: false
  },
  {
    id: 4,
    name: 'Done',
    edited: false
  }
]
