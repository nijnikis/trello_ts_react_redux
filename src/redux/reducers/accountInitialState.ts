import {
  IAccountState
} from '../../types'

export const accountInitialState: IAccountState = {
  name: null,
  mainTheme: '#000000',
  textTheme: '#000000'
}
