import {
  ITaskPopupState,
  IToggleTask
} from '../../../types'

export default function toggleTaskHandler(state: ITaskPopupState, action: IToggleTask) {
  return {
    taskOpenedId: state.taskOpenedId ? null : action.payload
  }
}
